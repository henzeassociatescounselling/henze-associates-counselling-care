Ros and I established Henze and Associates Counselling and Care in 1998 in Calgary, Alberta. We saw a need for a legitimate psychological counselling service combined with a solid Christian foundation for those clients whose spirituality is an integral part of their healing journey. I am a registered psychologist, and the focus of my graduate training was on sex and relationship therapy. At Henze and Associates, we work with a diverse team of psychologists, counselors, and other mental health professionals out of two locations in Calgary while collaborating and referring to a larger team spread over Southern Alberta.

For over 20 years, we have been helping individuals, couples, families, and people of all genders, faiths, racial heritages, and sexual orientations find healing. Our areas of expertise are Couple Relationship/Marriage and Sexuality, Addiction, and Trauma. Still, we also work with people on issues ranging from PTSD to anxiety and mood disorders and everything in between.

In addition to providing therapy, I have coordinated a city-wide, volunteer-staffed organization helping people and couples from all walks of life address sexual trauma and overcome compulsive behaviors and chemical dependencies. I have written extensively and taught in churches, community organizations, business executive training, and have provided professional development training in the diagnosis and treatment of human sexuality disorders to Canadian counsellors and psychotherapists.

Because each client is unique, we offer a wide variety of therapies, including:
Person-centered therapy
Family systems therapy
Emotion-focused therapy
Emotion-focused therapy for couples with trauma
Internal family systems therapy
Cognitive-behavioural therapy
Psychoeducational training
Mindfulness-based cognitive therapy
Trauma-focused addictions treatment
Sex therapy

We base our approach on whatever our client needs, and we have many different skills and techniques from which to choose. Some examples include:
Cognitive behavioral
Meditative
Mindful
Christian counselling and connection with God
High-level trauma therapy techniques
Sometimes a client needs nothing more than a conversation, to hear their words reflected and help with reinterpreting their issue or thought process

We believe that a client can become their own best therapist with the right tools and skills, so we offer practical suggestions that can be done at home between sessions (yes, especially for sex therapy!). Healing is often a lifelong process that cannot be rushed, but this does not mean that a client needs to be in counseling for the rest of their life. Our clients learn the skills necessary to experience healing and freedom both in and out of the office (or course or video call).

Everyone is worthy and capable of healing and deserves a safe space to explore their healing process. If you or someone you know is ready to start on the path to healing, please reach out and book your session today!

Website : https://henze-associates.com